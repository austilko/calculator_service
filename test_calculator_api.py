# -*- coding: utf-8 -*-

import requests
import json


def test_post_headers_body_json():
    url = 'http://127.0.0.1:5000/calculator'
    payloads_lsl = [
        {'first_num': 100, 'second_num': 50, 'operation': '/', 'answer': 2},
        {'first_num': 10, 'second_num': 50, 'operation': '/', 'answer': 0.2},
        {'first_num': 10, 'second_num': 50, 'operation': '*', 'answer': 500},
        {'first_num': 10, 'second_num': -50, 'operation': '*', 'answer': -500},
        {'first_num': 100, 'second_num': 50, 'operation': '-', 'answer': 50},
        {'first_num': 10, 'second_num': 50, 'operation': '-', 'answer': -40},
        {'first_num': 0, 'second_num': 0, 'operation': '+', 'answer': 0},
        {'first_num': 100, 'second_num': 50, 'operation': '+', 'answer': 150}
        ]
    headers = {'Content-Type': 'application/json'}

    for payload in payloads_lsl:
        resp = requests.post(url, headers=headers, data=json.dumps(payload, indent=4))
        assert resp.status_code == 201
        resp_body = resp.json()
        assert resp_body['result'] == payload['answer']
        print(resp.text)
