REST сервис "калькулятор" на microframework Flask и автотесты для него framework Pytest
Сервис принимает POST-запросы с данными в Body {'first_num': 100, 'second_num': 50, 'operation': '/'} и возвращает
ответ {'result': 2}
Причины выбора Pytest:
1. он используется в компании
2. на нём было просто реализовать требуемые автотесты

# У становить python3-pip
apt-get install python3-pip

# Клонируем проект из репозитория 
git clone git@gitlab.com:austilko/calculator_service.git

# Переходим в папку проекта
cd calculator_service/

# Установка требуемых модулей
pip install -r requirements.txt

# Для запуска сервиса
python3 flask_rest_calculator.py

# Для запуска автотестов(автотесты в файле test_calculator_api.py )
pytest