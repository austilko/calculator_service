# -*- coding: utf-8 -*-

from flask import Flask, request
from flask_restful import Resource, Api


app = Flask(__name__)
api = Api(app)


class Calculator(Resource):
    def post(self):
        def add(x, y):
            return x + y

        def subtract(x, y):
            return x - y

        def multiply(x, y):
            return x * y

        def divide(x, y):
            return x / y
        data = request.get_json()
        if data["operation"] == "+":
            result = add(float(data["first_num"]), float(data["second_num"]))
        if data["operation"] == "-":
            result = subtract(float(data["first_num"]), float(data["second_num"]))
        if data["operation"] == "*":
            result = multiply(float(data["first_num"]), float(data["second_num"]))
        if data["operation"] == "/":
            result = divide(float(data["first_num"]), float(data["second_num"]))

        return {'result': result}, 201


api.add_resource(Calculator, '/calculator')

if __name__ == '__main__':
    app.run(debug=True)
